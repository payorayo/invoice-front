import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import { AppContainer } from './container/App/container';
import store  from './container/App/store';

ReactDOM.render(
  <Provider store={store}>
    <AppContainer />
  </Provider>,
  document.getElementById('root')
);
