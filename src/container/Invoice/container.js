import { connect } from "react-redux";
import { Invoice } from '../../components/Invoice';
import { handleInvoiceStepSaga } from "../App/actions";

const mapStateToProps = state => ({
    invoceData: state.invoceData,
    email: state.userData.email
});

export const InvoiceContainer = connect(
  mapStateToProps,
  { handleInvoiceStepSaga }
)(Invoice);
