import axios from 'axios';
import { PATH_API } from '../../config';

export const login = (email, password) => {
    return axios.get(`${PATH_API}/auth?email=${email}&password=${password}`)
        .then((data: any) => ({ body: data.data.body, code: data.status}))
        .catch((err: any) => ({ body:[], code: 404}));
};

export const invoice = (user_id) => {
    return axios.get(`${PATH_API}/invoice?user_id=${user_id}`)
        .then((data: any) => ({ body: data.data.body, code: data.status}))
        .catch((err: any) => ({ body:[], code: 404}));
};

export const article = (user_id) => {
    return axios.get(`${PATH_API}/article`)
        .then((data: any) => ({ body: data.data.body, code: data.status}))
        .catch((err: any) => ({ body:[], code: 404}));
};

export const client = (user_id) => {
    return axios.get(`${PATH_API}/client`)
        .then((data: any) => ({ body: data.data.body, code: data.status}))
        .catch((err: any) => ({ body:[], code: 404}));
};

export const paymentmethod = (user_id) => {
    return axios.get(`${PATH_API}/paymentmethod`)
        .then((data: any) => ({ body: data.data.body, code: data.status}))
        .catch((err: any) => ({ body:[], code: 404}));
};

export const createInvoice = (data) => {
 return fetch(`${PATH_API}/invoice`, {
   method: 'POST',
    headers: {
        "Content-Type": "text/plain"
    },
    body: JSON.stringify(data)
})
.then(response => response.json())
.catch((err: any) => ({ body:[], message: 'error'}));
};
