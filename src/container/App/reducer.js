import * as constants from './constants';

export const initialState = {
    userData: {
        id: 0,
        email: '',
        first_name: '',
        last_name: ''
    },
    password: '',
    email: '',
    step: 0,
    isError: false,
    invoceData: [],
    article: [],
    client: [],
    payment: [],
    client_id: '',
    payment_method_id: '',
    note: '',
    newArticle: []
};

function loginRedoucer (state=initialState, action) {
    let temp;
    switch (action.type) {
        case constants.LOGIN_AUTH_REDUCER:
            return {
                ...state,
                userData: action.payload.data,
                step: action.payload.step,
                isError: action.payload.isError
            }; 
        case constants.LOGIN_INPUT_EMAIL_REDUCER:
            return {
                ...state,
                email: action.payload.value
            }
        
        case constants.LOGIN_INPUT_PASSWORD_REDUCER:
            return {
                ...state,
                password: action.payload.value
            }
        case constants.INVOICE_GET_REDUCER:
            return {
                ...state,
                invoceData: action.payload.data,
                password: '',
                email: ''
            }; 
        case constants.INVOICE_STEP_REDUCER:
            return {
                ...state,
                step: action.payload.step,
                client_id: 0,
                payment_method_id: 0,
                note: '',
                newArticle: [],
                isError: false
            }; 
        case constants.START_REDUCER:
            return {
                ...state,
                article: action.payload.article,
                client: action.payload.client,
                payment: action.payload.payment
            };
        case constants.INVOICE_INPUT_CLIENT_ID_REDUCER:
            return {
                ...state,
                client_id: action.payload.value
            }
        case constants.INVOICE_INPUT_PAYMENT_ID_REDUCER:
            return {
                ...state,
                payment_method_id: action.payload.value
            }
        case constants.INVOICE_INPUT_NOTE_REDUCER:
            return {
                ...state,
                note: action.payload.value
            }
        case constants.INVOICE_ADD_ARTICLE_REDUCER:
            return {
                ...state,
                newArticle: [ ...state.newArticle, {article_id: 0, amount: 0}]
            }
        case constants.INVOICE_UPDATE_ARTICLE_ID_REDUCER:
            temp = state.newArticle.map((item, index) =>{
                if (index !== action.payload.index) {
                    return item;
                }
                return {
                    ...item,
                    article_id: action.payload.value
                }
            });
            
            return {
                ...state,
                newArticle: temp
            } 
        case constants.INVOICE_UPDATE_AMOUNT_REDUCER:
            temp = state.newArticle.map((item, index) =>{
                if (index !== action.payload.index) {
                    return item;
                }
                return {
                    ...item,
                    amount: action.payload.value
                }
            });
            
            return {
                ...state,
                newArticle: temp
            } 

        case constants.INVOICE_REMOVE_ARTICLE_REDUCER:
            temp = [...state.newArticle.slice(0, action.payload.index), ...state.newArticle.slice(action.payload.index + 1)]
            return {
                ...state,
                newArticle: temp
            }
        case constants.INVOICE_SAVE_REDUCER:
            return {
                ...state,
                isError: true
            }; 
        default:
            return state;
    }
}

export { loginRedoucer };