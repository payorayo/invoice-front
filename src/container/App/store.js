import { createStore, compose, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { loginRedoucer, initialState } from './reducer';
import { rootSaga } from './saga';

const composeEnhancer = compose;

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  loginRedoucer,
  initialState,
  composeEnhancer(applyMiddleware(sagaMiddleware))
)
sagaMiddleware.run(rootSaga)

export default store;