import { call, put, takeLatest } from 'redux-saga/effects';
import * as actions from './actions';
import * as constants from './constants';
import { login, invoice, article, client, paymentmethod, createInvoice } from './api';

function* handleInvoiceSave(action){
    const {
      payload: { user_id, client_id, payment_method_id, note, newArticle }
    } = action;
    
    const response = {
        client_id,
        user_id,
        payment_method_id,
        note,
        articles: newArticle
    };
    const res = yield call(createInvoice, response);
    if(res.message === 'OK'){
        yield put(actions.handleInvoiceGetSaga({
                user_id: user_id,
        }));
        yield put(actions.handleInvoiceStepSaga({
            step: 1
        }));
    }
    else {
        yield put(actions.handleInvoiceSaveReducer());
    }
}

function* handleInvoiceRemoveArticle(action){
        const {
      payload: { index }
    } = action;
    yield put(actions.handleInvoiceRemoveArticleReducer({
        index: Number(index.replace('remove', ''))
    }));
}

function* handleInvoiceAddArticle(){
    yield put(actions.handleInvoiceAddArticleReducer());
}

function* handleStart(){
    const resArticle = yield call(article);
    const resClient = yield call(client);
    const resPaymentmethod = yield call(paymentmethod);

    yield put(actions.handleStartReducer({
        article: resArticle.body,
        client: resClient.body,
        payment: resPaymentmethod.body
    }));
}

function* handleInvoiceStep(action){
    const {
      payload: { step }
    } = action;

    yield put(actions.handleInvoiceStepReducer({
        step  
    }));
}


function* handleInvoiceGet(action){
    const {
      payload: { user_id }
    } = action;

    const res = yield call(invoice, user_id);
    yield put(actions.handleInvoiceGetReducer({
        data: res.body    
    }));
}

function* handleLoginAuth(action){
    const {
      payload: { email, password }
    } = action;
    const res = yield call(login, email, password);
    if(res.code === 200){
        yield put(actions.handleLoginAuthReducer({
            data: res.body[0],
            step: 1,
            isError: false
        }));

        yield put(actions.handleInvoiceGetSaga({
            user_id: res.body[0].id,
        }));
    }
    else{
        yield put(actions.handleLoginAuthReducer({
            data: {},
            step: 0,
            isError: true
        }));
    }

}

function* handleLoginInput(action) {
    const {
      payload: { index, value, id }
    } = action;
    switch (index) {
        case 'password':
                yield put(actions.handleLoginInputPasswordReducer({
                    value
                }));
            break;
    
        case 'email':
            yield put(actions.handleLoginInputEmailReducer({
                    value
            }));
            break;
        case 'client_id':
            yield put(actions.handleInvoiceInputClientIdSaga({
                    value
            }));
            break;
        case 'payment_method_id':
            yield put(actions.handleInvoiceInputPaymentIdSaga({
                    value
            }));
            break;
        case 'note':
            yield put(actions.handleInvoiceInputNoteSaga({
                    value
            }));
            break;

        case 'article_id':
            yield put(actions.handleInvoiceUpdateArticleIdReducer({
                value,
                index: Number(id.replace(index, ''))
            }));
            break;
        case 'amount':
            yield put(actions.handleInvoiceUpdateAmountReducer({
                value,
                index: Number(id.replace(index, ''))
            }));
            break;
        default:
            break;
    }
}

export function* rootSaga() {
    yield call(handleStart);
    yield takeLatest(constants.LOGIN_AUTH_SAGA, handleLoginAuth);
    yield takeLatest(constants.LOGIN_INPUT_SAGA, handleLoginInput);
    yield takeLatest(constants.INVOICE_GET_SAGA, handleInvoiceGet);
    yield takeLatest(constants.INVOICE_STEP_SAGA, handleInvoiceStep);
    yield takeLatest(constants.INVOICE_ADD_ARTICLE_SAGA, handleInvoiceAddArticle);
    yield takeLatest(constants.INVOICE_REMOVE_ARTICLE_SAGA,  handleInvoiceRemoveArticle);
    yield takeLatest(constants.INVOICE_SAVE_SAGA, handleInvoiceSave);
}