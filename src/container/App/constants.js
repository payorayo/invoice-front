export const LOGIN_INPUT_SAGA = `loginInputSaga`;

export const LOGIN_INPUT_EMAIL_REDUCER = `loginInputEmailReducer`;

export const LOGIN_INPUT_PASSWORD_REDUCER = `loginInputPasswordReducer`;

export const LOGIN_AUTH_SAGA = `loginAuthSaga`;

export const LOGIN_AUTH_REDUCER = `loginAuthReducer`;

export const INVOICE_GET_SAGA = `invoiceGetSaga`;

export const INVOICE_GET_REDUCER = `invoiceGetReducer`;

export const INVOICE_STEP_SAGA = `invoiceStepSaga`;

export const INVOICE_STEP_REDUCER = `invoiceStepReducer`;

export const START_REDUCER = `startReducer`;

export const INVOICE_INPUT_CLIENT_ID_REDUCER = `invoiceInputClientIDReducer`;

export const INVOICE_INPUT_PAYMENT_ID_REDUCER = `invoiceInputPaymentIDReducer`;

export const INVOICE_INPUT_NOTE_REDUCER = `invoiceInputNoteReducer`;

export const INVOICE_ADD_ARTICLE_SAGA = `invoiceAddArticleSaga`;

export const INVOICE_ADD_ARTICLE_REDUCER = `invoiceAddArticleReducer`;

export const INVOICE_UPDATE_ARTICLE_ID_REDUCER = `invoiceUpdateArticleIdReducer`;

export const INVOICE_UPDATE_AMOUNT_REDUCER = `invoiceUpdateAmountReducer`;

export const INVOICE_REMOVE_ARTICLE_SAGA = `invoiceRemoveArticleSaga`;

export const INVOICE_REMOVE_ARTICLE_REDUCER = `invoiceRemoveReducerSaga`;

export const INVOICE_SAVE_SAGA = `invoiceSaveSaga`;

export const INVOICE_SAVE_REDUCER = `invoiceSaveReducer`;