import { connect } from "react-redux";
import App from '../../components/App';

const mapStateToProps = state => ({
    step: state.step
});

export const AppContainer = connect(
  mapStateToProps
)(App);
