import * as constants from './constants';

export const handleLoginAuthSaga = payload => ({
  type: constants.LOGIN_AUTH_SAGA,
  payload: payload
});

export const handleLoginAuthReducer = payload => ({
  type: constants.LOGIN_AUTH_REDUCER,
  payload
});

export const handleLoginInputSaga = payload => ({
  type: constants.LOGIN_INPUT_SAGA,
  payload: payload
});

export const handleLoginInputEmailReducer = payload => ({
  type: constants.LOGIN_INPUT_EMAIL_REDUCER,
  payload: payload
});

export const handleLoginInputPasswordReducer = payload => ({
  type: constants.LOGIN_INPUT_PASSWORD_REDUCER,
  payload: payload
});

export const handleInvoiceGetSaga = payload => ({
  type: constants.INVOICE_GET_SAGA,
  payload: payload
});

export const handleInvoiceGetReducer = payload => ({
  type: constants.INVOICE_GET_REDUCER,
  payload: payload
});

export const handleInvoiceStepSaga = payload => ({
  type: constants.INVOICE_STEP_SAGA,
  payload: payload
});

export const handleInvoiceStepReducer = payload => ({
  type: constants.INVOICE_STEP_REDUCER,
  payload: payload
});

export const handleStartReducer = payload => ({
  type: constants.START_REDUCER,
  payload: payload
});

export const handleInvoiceInputClientIdSaga = payload => ({
  type: constants.INVOICE_INPUT_CLIENT_ID_REDUCER,
  payload: payload
});

export const handleInvoiceInputPaymentIdSaga = payload => ({
  type: constants.INVOICE_INPUT_PAYMENT_ID_REDUCER,
  payload: payload
});

export const handleInvoiceInputNoteSaga = payload => ({
  type: constants.INVOICE_INPUT_NOTE_REDUCER,
  payload: payload
});

export const handleInvoiceAddArticleSaga = payload => ({
  type: constants.INVOICE_ADD_ARTICLE_SAGA,
  payload: payload
});

export const handleInvoiceAddArticleReducer = payload => ({
  type: constants.INVOICE_ADD_ARTICLE_REDUCER,
  payload: payload
});

export const handleInvoiceUpdateArticleIdReducer = payload => ({
  type: constants.INVOICE_UPDATE_ARTICLE_ID_REDUCER,
  payload: payload
});

export const handleInvoiceUpdateAmountReducer = payload => ({
  type: constants.INVOICE_UPDATE_AMOUNT_REDUCER,
  payload: payload
});

export const handleInvoiceRemoveArticleSaga = payload => ({
  type: constants.INVOICE_REMOVE_ARTICLE_SAGA,
  payload: payload
});

export const handleInvoiceRemoveArticleReducer = payload => ({
  type: constants.INVOICE_REMOVE_ARTICLE_REDUCER,
  payload: payload
});

export const handleInvoiceSaveSaga = payload => ({
  type: constants.INVOICE_SAVE_SAGA,
  payload: payload
});

export const handleInvoiceSaveReducer = payload => ({
  type: constants.INVOICE_SAVE_REDUCER,
  payload: payload
});



