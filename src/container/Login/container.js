import { connect } from "react-redux";
import { handleLoginAuthSaga, handleLoginInputSaga } from "../App/actions";
import { Login } from '../../components/Login';

const mapStateToProps = state => ({
    password: state.password,
    email: state.email,
    isError: state.isError
});

export const LoginContainer = connect(
  mapStateToProps,
  {handleLoginAuthSaga, handleLoginInputSaga}
)(Login);
