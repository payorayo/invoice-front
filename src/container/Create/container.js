import { connect } from "react-redux";
import { Create } from '../../components/Create';
import { handleInvoiceStepSaga, handleLoginInputSaga, handleInvoiceAddArticleSaga, handleInvoiceRemoveArticleSaga, handleInvoiceSaveSaga } from "../App/actions";

const mapStateToProps = state => ({
    user_id: state.userData.id,
    email: state.userData.email,
    article: state.article,
    client: state.client,
    payment: state.payment,
    client_id: state.client_id,
    payment_method_id: state.payment_method_id,
    note: state.note,
    newArticle: state.newArticle,
    isError: state.isError
});

export const CreateContainer = connect(
  mapStateToProps,
  { handleInvoiceStepSaga, handleLoginInputSaga, handleInvoiceAddArticleSaga,handleInvoiceRemoveArticleSaga, handleInvoiceSaveSaga }
)(Create);
