import React from 'react';
import PropTypes from 'prop-types';

const Create = props => {
    const { 
        user_id, 
        email, 
        handleInvoiceStepSaga, 
        client_id,
        payment_method_id,
        note,
        newArticle,
        handleLoginInputSaga,
        handleInvoiceAddArticleSaga,
        handleInvoiceRemoveArticleSaga,
        isError,
        handleInvoiceSaveSaga
    } = props; 
    const handleClick= () => {
        handleInvoiceStepSaga({
            step: 1
        });
    };

    const handleSaveClick= () => {
        handleInvoiceSaveSaga({
            user_id, 
            client_id, 
            payment_method_id, 
            note, 
            newArticle
        });
    };

    const handleInputChange = event => {
        const { target } = event;
        handleLoginInputSaga({
            index: target.name,
            value: target.value,
            id: target.id
        });
    };

    const eventDeleteArticle= event => {
        const { target } = event;
        handleInvoiceRemoveArticleSaga({
            index: target.id
        });
    };

    const eventNewArticle= () => {
        handleInvoiceAddArticleSaga();
    };

    const showSelectClient = () => props.client.map(item => <option value={item.id}>{item.rfc} {item.name}</option>);

    const selectOption = (option, id) => (option === id);

    const showSelectPayment = () => props.payment.map(item => <option value={item.id}>{item.name}</option>);
        

    const showSelectArticle = () => props.article.map(item =>(
        <option value={item.id} >{item.name}  $ {item.price}</option>
    ));

    const showFormArticles = () =>props.newArticle.map((item, i) => (
            <div className="form-row" key={i}>
                <div className="form-group col-md-6">
                <label htmlFor="inputState">Article</label>
                    <select id={`article_id${i}`} name="article_id" className="form-control" defaultValue={item.article_id} onChange={e => handleInputChange(e)} >
                        <option value="0">Choose...</option>
                        {showSelectArticle()}
                    </select>
                </div>
                <div className="form-group col-md-4">
                    <label htmlFor="inputZip">Ammount</label>
                    <input type="number" id={`amount${i}`} className="form-control" name="amount" value={item.amount} onChange={e => handleInputChange(e)} />
                </div>
                <div className="form-group col-md-2">
                    <button type="button" id={`remove${i}`} className="btn btn-sm btn-outline-secondary" onClick={e => eventDeleteArticle(e)}>remove</button>
                </div>
            </div>
    ));

    const showMessage = () => {
        if(isError){
        return (<div class="alert alert-danger" role="alert">
            There are fields with incorrent values. Try Again please
            </div>);
        }
        return '';
    };

    return (
       <div className="container-fluid">
            <div className="row">
                <div className="col-md-8">
                    <h1 className="h2">New Invoice - {email}</h1>
                </div>
                <div className="col-md-4">
                    <button type="button" className="btn btn-sm btn-outline-secondary" onClick={handleClick}>Back</button>
                </div>
            </div>
            {showMessage()}
            <div className="form-row">
                <label htmlFor="inputState">client</label>
                <select name="client_id" className="form-control" defaultValue={client_id} onChange={e => handleInputChange(e)}>
                    <option value="0">Choose...</option>
                    {showSelectClient()}
                </select>
            </div>
            <div className="form-row">
                <label htmlFor="inputState">Payment Method</label>
                <select name="payment_method_id" className="form-control" defaultValue={payment_method_id} onChange={e => handleInputChange(e)}>
                    <option value="0">Choose...</option>
                    {showSelectPayment()}
                </select>
            </div>
            <div className="form-group">
                <label>Note</label>
                <textarea className="form-control" id="note" name="note" rows="3" onChange={e => handleInputChange(e)} value={note}></textarea>
            </div>
            <div className="row">
                <div className="col-md-12">
                    <h3>Add Articles</h3>
                    <button type="button" className="btn btn-sm btn-outline-secondary" onClick={eventNewArticle}>New Article</button>
                </div>
            </div>
            {showFormArticles()}
            <div className="row">
                <button type="submit" className="btn btn-primary" onClick={handleSaveClick}>Create</button>
            </div>
        </div>
    );
};

Create.propTypes = {
    user_id: PropTypes.number.isRequired,
    email: PropTypes.string.isRequired,
    handleInvoiceStepSaga: PropTypes.func.isRequired,
    handleLoginInputSaga: PropTypes.func.isRequired,
    handleInvoiceAddArticleSaga: PropTypes.func.isRequired,
    handleInvoiceRemoveArticleSaga: PropTypes.func.isRequired,
    handleInvoiceSaveSaga: PropTypes.func.isRequired,
    article: PropTypes.any,
    client: PropTypes.any,
    payment: PropTypes.any,
    client_id: PropTypes.number.isRequired,
    payment_method_id: PropTypes.number.isRequired,
    note: PropTypes.string.isRequired,
    newArticle: PropTypes.any,
    isError: PropTypes.bool.isRequired,
}

export { Create };