import React from 'react';
import PropTypes from 'prop-types';
import './Login.css';

const Login = props => {
  const { handleLoginAuthSaga, handleLoginInputSaga, isError, email, password } = props;

  const handleInputChange = event => {
    const { target } = event;

    handleLoginInputSaga({
      index: target.name,
      value: target.value
    });
  };

  const handleClick= () => {
    handleLoginAuthSaga({
      email,
      password
    });
  }

  const showMessage = () => {
    if(isError){
      return (<div class="alert alert-danger" role="alert">
          Email and Password are incorrect. Try Again please
        </div>);
    }
    return '';
  };

  return (
      <form className="form-signin">
        {showMessage()}
        <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
        <label htmlFor ="email" className="sr-only">Email</label>
        <input 
          type="email" 
          name="email" 
          className="form-control" 
          placeholder="Email" 
          onChange={e => handleInputChange(e)}
          value={email} />
        <label htmlFor ="password" className="sr-only">Password</label>
        <input 
          type="password" 
          name="password" 
          className="form-control" 
          placeholder="Password" 
          onChange={e => handleInputChange(e)}
          value={password}
          autoComplete="on" />
        <button className="btn btn-lg btn-primary btn-block" type="button" onClick={handleClick}>Sign in</button>
      </form>
  );
};

Login.propTypes = {
  handleLoginAuthSaga: PropTypes.func.isRequired,
  handleLoginInputSaga: PropTypes.func.isRequired,
  isError: PropTypes.bool.isRequired,
  email: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired
}


export { Login };
