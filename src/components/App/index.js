import React from 'react';
import PropTypes from 'prop-types';
import { LoginContainer } from '../../container/Login/container';
import { InvoiceContainer } from '../../container/Invoice/container';
import { CreateContainer } from '../../container/Create/container';

const App = props => {
  const { step } = props;
  
  const router =() => {
    switch(step){
      case 2:
        return <CreateContainer />; 
      case 1:
        return <InvoiceContainer />; 
      default:
        return <LoginContainer />;
    }
  };
  
  return router();
};

App.propTypes = {
  step: PropTypes.number.isRequired
}

export default App;
