import React from 'react';
import PropTypes from 'prop-types';
import './dashboard.css';

const Invoice = props => {
    const { invoceData, email, handleInvoiceStepSaga } = props; 

    const handleClick= () => {
        handleInvoiceStepSaga({
            step: 2
        });
    }

    const showList = () => invoceData.map(invoice => (<tr>
        <td>{invoice.id}</td>
        <td>{invoice.clientname}</td>
        <td>{invoice.payment}</td>
        <td>{invoice.note}</td>
        <td>{invoice.articles}</td>
    </tr>));

    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-8">
                    <h1 className="h2">Invoices - {email}</h1>
                </div>
                <div className="col-md-4">
                    <button type="button" class="btn btn-sm btn-outline-secondary" onClick={handleClick}>New</button>
                </div>
            </div>
            <div className="row">
                    <div className="table-responsive">
                        <table className="table table-striped table-sm">
                        <thead>
                            <tr>
                            <th>#</th>
                            <th>Client</th>
                            <th>Payment Method</th>
                            <th>Notes</th>
                            <th># Articles</th>
                            </tr>
                        </thead>
                        <tbody>
                            {showList()}
                        </tbody>
                        </table>
                    </div>
            </div>
        </div>
    );
}

Invoice.propTypes = {
    email: PropTypes.string.isRequired,
    invoceData: PropTypes.any,
    handleInvoiceStepSaga: PropTypes.func.isRequired
}

export { Invoice };